<?php
    $title = "Latihan PHP";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <title>Document</title>
</head>
<body>
    <h2>Latihan PHP</h2>
    <?php
        echo "<h3>Soal 1</h3>";
        $kalimat1 = "PHP is never olddd";
        echo "Kalimat 1 : ". $kalimat1. "<br>";
        echo "panjang string:". strlen($kalimat1). "<br>";
        echo "jumlah kata:". str_word_count($kalimat1). "<br>";

        echo "<h3>Soal 2</h3>";
        $kalimat2 ="halo dunia";
        echo "kalimat :". $kalimat2. "<br>";
        echo "kata 1:". substr($kalimat2,0,4)."<br>";
        echo "kata 2:". substr($kalimat2,5,5);"<br>";

        echo "<h3>Soal 3</h3>";
        $kalimat3 ="aku suka ayam";
        echo "kalimat 3:". $kalimat3.  "<br>";
        echo "kalimat 3 diubah menjadi:". str_replace("ayam","daging",$kalimat3);
    ?>
</body>
</html> 