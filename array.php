<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <title>Array Latihan</title>
</head>
<body>
    <h2>Array</h2>
    <?php
        echo "<h3> Soal 1 </h3>";

        $Kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" ];
        $Adults= ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
        print_r($Kids). "<br>";
        print_r($Adults). "<br>";

        echo "<h3> Soal 2 </h3>";
        echo "Total Adults:". count($Adults). "<br>";
        echo "<ol>";
        echo "<li>". $Adults[0]."</li>";
        echo "<li>". $Adults[1]."</li>";
        echo "<li>". $Adults[2]."</li>";
        echo "<li>". $Adults[3]."</li>";
        echo "<li>". $Adults[4]."</li>";
        echo "</ol>";

        echo "Total Kids:". count($Kids). "<br>";
        echo "<ol>";
        echo "<li>". $Kids[0]."</li>";
        echo "<li>". $Kids[1]."</li>";
        echo "<li>". $Kids[2]."</li>";
        echo "<li>". $Kids[3]."</li>";
        echo "<li>". $Kids[4]."</li>";
        echo "<li>". $Kids[5]."</li>";
        echo "</ol>";

        echo "<h3> Soal 3 </h3>";
        $list = [
            ["Name"=>"Will Byers","Age"=>"12","Aliases"=>"Will the Wise","Status"=>"Alive"],
            ["Name"=>"Mike Wheeler","Age"=>"12","Aliases"=>"Dungeon Master","Status"=>"Alive"],
            ["Name"=>"Jim Hooper","Age"=>"43","Aliases"=>"Chief Hopper","Status"=>"Deceased"],
            ["Name"=>"Eleven","Age"=>"12","Aliases"=>"El","Status"=>"Alive"]
            
        ];

        echo"<pre>";
        print_r($list);
        echo"</pre>";
    

     
    ?>
</body>
</html> 